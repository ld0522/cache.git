package com.ld.demo;

import com.ld.test.J2CacheApplication;
import com.ld.test.Redisson.service.RedisCacheService;
import com.ld.test.entity.User;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressWarnings("ALL")
/**
 * @Description :
 * @author     :LD
 */
@Log4j2
@SpringBootTest(classes = J2CacheApplication.class)
@RunWith(SpringRunner.class)
public class RedisCacheServiceTest {
    @Autowired
    private RedisCacheService redisCacheService;
    /**
     * 支持多线程测试
     */
    @Before
    public void before(){
        redisCacheService.setCurrentTest(true);
    }

    @Test
    public void testTryRateLimiter() throws InterruptedException {
        //使用线程池 初始化50个线程，让每一个线程执行一个请求
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        //初始化等待数50
        //外部阻塞
        CountDownLatch countDownLatch = new CountDownLatch(50);
        //内部阻塞
        CyclicBarrier cyclicBarrier = new CyclicBarrier(50);
        for (int i = 0; i < 50; i++) {
            executorService.execute(()->{
                //阻塞
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log.info("请求线程"+Thread.currentThread().getId()+"时间；{}",new Date());
                redisCacheService.tryRateLimiter("tryRateLimiter_101010101");
                log.info("处理线程"+Thread.currentThread().getId()+"时间：{}",new Date());
                //等待数-1，挂起当前线程
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
    }

    @Test
    public void testSingleCache() throws InterruptedException {

        String key ="testSingleCache_101010101";
        //初始化50个线程，让每个线程执行一个请求
        ExecutorService executorServicePool = Executors.newFixedThreadPool(50);
        //初始化等待数：50
        //外部阻塞
        CountDownLatch countDownLatch = new CountDownLatch(50);
        //内部阻塞
        CyclicBarrier cyclicBarrier = new CyclicBarrier(50);
        for (int i = 0; i < 50; i++) {
            executorServicePool.execute(()-> {
                //阻塞
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log.info("请求线程"+Thread.currentThread().getId()+"时间：{}",new Date());
                redisCacheService.singleCache(() -> {
                    log.info("执行数据查询");
                    return new User(1,"张三",12);
                }, key);
                log.info("处理线程" + Thread.currentThread().getId() + "时间：{}", new Date());
                //等待数-1，挂起当前线程
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
    }

    @Test
    public void testDeleSingleCache() throws InterruptedException {
        String key ="testAddSingleCache_101010101";
        redisCacheService.deleSingleCache(key);
    }


    @Test
    public void testListCache() throws InterruptedException {

        String key ="testListCache_101010101";
        //初始化50个线程，让每个线程执行一个请求
        ExecutorService executorServicePool = Executors.newFixedThreadPool(50);
        //外部阻塞
        CountDownLatch countDownLatch = new CountDownLatch(50);
        //内部阻塞
        CyclicBarrier cyclicBarrier = new CyclicBarrier(50);
        for (int i = 0; i < 50; i++) {
            executorServicePool.execute(()-> {
                //阻塞线程等待
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log.info("请求线程"+Thread.currentThread().getId()+"时间：{}",new Date());
                redisCacheService.listCache(() -> {
                    log.info("执行数据查询");
                    List<User> list = new ArrayList<>();
                    list.add(new User(1,"张三",12));
                    return list;
                }, key);
                log.info("处理线程" + Thread.currentThread().getId() + "时间：{}", new Date());
                //等待数-1，
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
    }


    @Test
    public void testDeleListCache() throws InterruptedException {
        String key ="testListCache_101010101";
        redisCacheService.deleSingleCache(key);

    }

    @Test
    public void testMapCache() throws InterruptedException {

        String key ="testMapCache_101010101";
        ///初始化50个线程，让每个线程执行一个请求
        ExecutorService executorServicePool = Executors.newFixedThreadPool(50);
        //外部阻塞
        CountDownLatch countDownLatch = new CountDownLatch(50);
        //内部阻塞
        CyclicBarrier cyclicBarrier = new CyclicBarrier(50);
        for (int i = 0; i < 50; i++) {
            executorServicePool.execute(()-> {
                log.info("请求线程"+Thread.currentThread().getId()+"时间：{}",new Date());
                //阻塞线程等待
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                redisCacheService.mapCache(() -> {
                    log.info("执行数据查询");
                    Map<Integer,User> map = new HashMap<>();
                    map.put(1111, new User(1,"张三",12));
                    return map;
                }, key);
                log.info("处理线程" + Thread.currentThread().getId() + "时间：{}", new Date());
                //等待数-1，挂起当前线程
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
    }

    @Test
    public void testDeleMapCache() throws InterruptedException {

        String key ="testMapCache_101010101";
        redisCacheService.deleMapCache(key);
    }

}
