package com.ld.test;

import com.ld.test.Redisson.config.RedissonConfig;
import com.ld.test.j2cache.annotation.EnableCache;
import org.redisson.api.RedissonClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author :LD
 * @date :2021/6/5
 */
@SpringBootApplication
@EnableConfigurationProperties(RedissonConfig.class)
@EnableCache
public class J2CacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(J2CacheApplication.class,args);
        System.out.println("===j2Cache缓存应用启动===");
    }
}
