package com.ld.test.j2cache.annotation;

import com.ld.test.j2cache.aop.CacheMethodInterceptor;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 缓存启用开关
 * @author :ld
 * @date :2021/6/3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import(CacheMethodInterceptor.class)
public @interface EnableCache {
}
