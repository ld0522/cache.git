package com.ld.test.j2cache.annotation;

import java.lang.annotation.*;

/**
 * 缓存失效
 * @author :ld
 * @date :2021/6/3
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheEvictor {
    Cache[] value() default {};
}
