package com.ld.test.j2cache.annotation;

import java.lang.annotation.*;

/**
 * 缓存注解
 * @author :ld
 * @date :2021/6/3
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Cache {
    String region() default "rx";
    String key() default "";
    String params() default "";
}
