package com.ld.test.controller;

import com.ld.test.entity.User;
import com.ld.test.j2cache.annotation.Cache;
import com.ld.test.j2cache.annotation.CacheEvictor;
import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.CacheObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :LD
 * @date :2021/6/5
 */
@RestController
@RequestMapping("/cache/annotation")
public class MyController1 {
    /**
     * 获取所有用户信息
     * @return 用户列表
     */
    @GetMapping("/getInfos")
    @Cache(region = "rx",key = "myCache")
    public List<User> getInfos(){
        List<User> data = new ArrayList<User>();
        data.add(new User(1,"张三",24));
        data.add(new User(2,"李四",25));
        return data;
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("user/{id}")
    @CacheEvictor({@Cache(region = "rx",key = "myCache",params = "id")})
    public void delUserById(@PathVariable("id") Integer id){
        System.out.println("数据库删除逻辑启动");
    }
}
