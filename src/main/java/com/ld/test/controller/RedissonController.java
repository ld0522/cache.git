package com.ld.test.controller;

import com.ld.test.Redisson.service.RedisCacheService;
import com.ld.test.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author :LD
 * @date :2021/6/11
 */
@RestController
@RequestMapping("redisson")
public class RedissonController {
    @Autowired
    RedisCacheService redisCacheService;

    @GetMapping("user/{id}")
    public User getUserById(@PathVariable("id") Integer id){
        return redisCacheService.singleCache(()->{
            return new User(1,"赵六",21);
        },"redissonKey");
    }
}
