package com.ld.test.Redisson.basic;

import java.util.Map;

/**
 * @Description: Map结构
 */
@SuppressWarnings("ALL")
@FunctionalInterface
public interface MapData<K, V> {

    Map<K, V> getData();

}
