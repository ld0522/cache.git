package com.ld.test.Redisson.basic;

import java.util.List;

/**
 * @Description: List结构
 */
@SuppressWarnings("ALL")
@FunctionalInterface
public interface ListData<E> {

    List<E> getData();

}
