package com.ld.test.Redisson.basic;

/**
 * @Description: 对象结构
 */
@SuppressWarnings("ALL")
@FunctionalInterface
public interface SingleData<V> {

    V getData();

}
