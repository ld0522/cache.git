package com.ld.test.Redisson.config;

import lombok.Data;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@SuppressWarnings("ALL")
/**
 * @Description :
 * @author     :LD
 */
@ConfigurationProperties(prefix = "ld.redis")
@Data
public class RedissonConfig {
    /**
     * redis连接地址
     */
    private String nodes;

    /**
     * 获取连接超时时间
     */
    private int connectTimeout;

    /**
     * 最小空闲连接数
     */
    private int connectPoolSize;

    /**
     * 最小连接数
     */
    private int connectionMinimumidleSize;

    /**
     * 等待数据返回超时时间
     */
    private int timeout;

    /**
     * 刷新时间
     */
    private int retryInterval;

    @Bean(value = "redissonClient",destroyMethod = "shutdown")
    public RedissonClient redissonClient(){
        String[] nodeList = nodes.split(",");
        Config config = new Config();
        //单节点
        if (nodeList.length == 1) {
            config.useSingleServer().setAddress(nodeList[0])
                    .setConnectTimeout(connectTimeout)
                    .setConnectionMinimumIdleSize(connectionMinimumidleSize)
                    .setConnectionPoolSize(connectPoolSize)
                    .setTimeout(timeout);
            //集群节点
        } else {
            config.useClusterServers().addNodeAddress(nodeList)
                    .setConnectTimeout(connectTimeout)
                    .setRetryInterval(retryInterval)
                    .setMasterConnectionMinimumIdleSize(connectionMinimumidleSize)
                    .setMasterConnectionPoolSize(connectPoolSize)
                    .setSlaveConnectionMinimumIdleSize(connectionMinimumidleSize)
                    .setSlaveConnectionPoolSize(connectPoolSize)
                    .setTimeout(3000);
        }
        return Redisson.create(config);
    }
}
